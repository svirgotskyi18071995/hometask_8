# Створіть клас "Транспортний засіб"
# та підкласи "Автомобіль", "Літак", "Корабель", наслідувані від "Транспортний засіб".
# Наповніть класи атрибутами на свій розсуд.
tsk1 = '------------------------------------------ Task 1 -------------------------------------------------'
space = '---------------------------------------------------------------------------------------------------'
print(space)
print(tsk1)
print(space)
class vehicle:
    ability_to_move = True

class Car(vehicle):
    max_speed_on_road = 300  # km/h
    fuel_consumption = 12   #liters per 100km
class Plane(vehicle):
    max_speed_on_sky = 1800  # km/h
    flight_time = 2  # hours

class Ship(vehicle):
    max_speed_on_water = 30 #nodes
    kind_of_ship = 'Tanker'