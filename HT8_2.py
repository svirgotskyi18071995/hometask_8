# Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".

tsk2 = '------------------------------------------ Task 2 -------------------------------------------------'
space = '---------------------------------------------------------------------------------------------------'
print(space)
print(tsk2)
print(space)
class vehicle:
    ability_to_move = True

class Car(vehicle):
    max_speed_on_road = 300  # km/h
    fuel_consumption = 12  # liters per 100km
    def __init__(self,max_speed_on_road, fuel_consumption):
        self.max_speed_on_road = max_speed_on_road
        self.fuel_consumption = fuel_consumption


class Plane(vehicle):
    max_speed_on_sky = 1800  # km/h
    flight_time = 2  # hours
    def __init__(self,max_speed_on_sky, flight_time):
        self.max_speed_on_sky = max_speed_on_sky
        self.flight_time = flight_time

class Ship(vehicle):
    max_speed_on_water = 30 #nodes
    kind_of_ship = 'Tanker'
    def __init__(self,max_speed_on_water, kind_of_ship):
        self.max_speed_on_water = max_speed_on_water
        self.kind_of_ship = kind_of_ship

Audi_A6 = Car(max_speed_on_road=280, fuel_consumption= 12)
Volkswagen_Passat = Car(max_speed_on_road=250, fuel_consumption= 10)
Mercedes_Benz_S600 = Car(max_speed_on_road=275, fuel_consumption= 20)
F_16 = Plane( max_speed_on_sky=2400,flight_time= 5)
AN_225 = Plane(max_speed_on_sky= 800, flight_time= 18)
Boeing_787_9 = Plane(max_speed_on_sky= 900, flight_time= 19)
Titanic = Ship(max_speed_on_water= 23, kind_of_ship= 'Cruise Liner')
Bismarck = Ship(max_speed_on_water= 30, kind_of_ship= 'Battleship')
USS_Gerald_Ford = Ship(max_speed_on_water= 35, kind_of_ship= 'Aircraft carrier')
